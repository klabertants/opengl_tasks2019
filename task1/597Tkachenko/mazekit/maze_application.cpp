#include "maze_application.hpp"

#include "meshes/parallelepiped.hpp"

#include <iostream>

namespace mazekit {

namespace {

static const std::string MAZE_FILE_PATH = "597TkachenkoData1/maze.txt";
static const std::string SHADER_VERT_FILE_PATH = "597TkachenkoData1/shader.vert";
static const std::string SHADER_FRAG_FILE_PATH = "597TkachenkoData1/shader.frag";

static const float MARGIN_WIDTH = 0.1f;

}

MazeApplication::MazeApplication() : 
	Application()
{
	_cameraMover = std::make_shared<OrbitCameraMover>();
	activeCamera_ = _cameraMover;

	maze_ = std::make_shared<mazekit::Maze>(MAZE_FILE_PATH);

	inactiveCamera_ = std::make_shared<MazeCameraMover>(maze_);
}

void MazeApplication::makeScene()
{
	Application::makeScene();

	shader_ = std::make_shared<ShaderProgram>(
		SHADER_VERT_FILE_PATH,
		SHADER_FRAG_FILE_PATH
	);

	xSize_ = maze_->width();
	ySize_ = maze_->height();

	initFloor(xSize_, ySize_);
	initWalls();
	initCeil();
}

void MazeApplication::draw()
{
	Application::draw();
	int width, height;

	glfwGetFramebufferSize(_window, &width, &height);
	glViewport(0, 0, width, height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader_->use();

	shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
	shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);

	shader_->setMat4Uniform("modelMatrix", floorMesh_->modelMatrix());
	floorMesh_->draw();

	for (const auto& wall : wallMeshes_) {
		shader_->setMat4Uniform("modelMatrix", wall->modelMatrix());
		wall->draw();
	}

	if (drawCeil_) {
		shader_->setMat4Uniform("modelMatrix", ceilMesh_->modelMatrix());
		ceilMesh_->draw();
	}
}

void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_V) {
			activeCamera_.swap(inactiveCamera_);
			_cameraMover = activeCamera_;
		}
		else if (key == GLFW_KEY_M) {
			drawCeil_ = !drawCeil_;
		}
		Application::handleKey(key, scancode, action, mods);
	}
}

void MazeApplication::initFloor(float xSize, float ySize) 
{
	floorMesh_ = mazekit::meshes::makeParallelepiped(xSize + MARGIN_WIDTH, ySize + MARGIN_WIDTH, MARGIN_WIDTH);
	floorMesh_->setModelMatrix(
		glm::translate(
			glm::mat4(1.0f), 
			glm::vec3(
				0.0f,
				0.0f, 
				0.0f
			)
		)
	);
}

void MazeApplication::initWalls() 
{
	for (int i = 0; i <= xSize_; ++i) {
		for (int j = 0; j <= ySize_; ++j) {
			if (maze_->angleWalls()[j][i]) {
				MeshPtr barMesh = mazekit::meshes::makeParallelepiped(MARGIN_WIDTH, MARGIN_WIDTH, 1.0f);
				barMesh->setModelMatrix(
					glm::translate(
						glm::mat4(1.0f),
						glm::vec3(
							i * 1.0f - 0.5f * xSize_, 
							j * 1.0f - 0.5f * ySize_,
							0.5f
						)
					)
				);
				wallMeshes_.push_back(barMesh);
			}
		}
	}

	for (int i = 0; i < xSize_; ++i) {
		for (int j = 0; j <= ySize_; ++j) {
			if (maze_->horizontalWalls()[j][i]) {
				MeshPtr xMesh = mazekit::meshes::makeParallelepiped(1.0f, MARGIN_WIDTH, 1.0f);
				xMesh->setModelMatrix(
					glm::translate(
						glm::mat4(1.0f), 
						glm::vec3(getWallCoordinates(i, j, true))
					)
				);
				wallMeshes_.push_back(xMesh);
			}
		}
	}

	for (int i = 0; i <= xSize_; ++i) {
		for (int j = 0; j < ySize_; ++j) {
			if (maze_->verticalWalls()[j][i]) {
				MeshPtr yMesh = mazekit::meshes::makeParallelepiped(MARGIN_WIDTH, 1.0f, 1.0f);
				yMesh->setModelMatrix(
					glm::translate(
						glm::mat4(1.0f), 
						glm::vec3(getWallCoordinates(i, j, false))
					)
				);
				wallMeshes_.push_back(yMesh);
			}
		}
	}

	for (int i = 0; i < xSize_; ++i) {
		for (int j = 0; j < ySize_; ++j) {
			if (maze_->mazeCells()[j][i]) {
				MeshPtr cell = mazekit::meshes::makeParallelepiped(1.0f - MARGIN_WIDTH, 1.0f - MARGIN_WIDTH, 1.0f);
				cell->setModelMatrix(
					glm::translate(
						glm::mat4(1.0f),
						glm::vec3(
							i * 1.0f + 0.5 - 0.5f * xSize_,
							j * 1.0f + 0.5 - 0.5f * ySize_,
							0.5f
						)
					)
				);
				wallMeshes_.push_back(cell);

			}
		}
	}
}

void MazeApplication::initCeil()
{
	ceilMesh_ = mazekit::meshes::makeParallelepiped(xSize_ + MARGIN_WIDTH, ySize_ + MARGIN_WIDTH, MARGIN_WIDTH);
	ceilMesh_->setModelMatrix(
		glm::translate(
			glm::mat4(1.0f), 
			glm::vec3(
				0.0f, 
				0.0f, 
				1.0f + MARGIN_WIDTH * 0.5f
			)
		)
	);
}

glm::vec3 MazeApplication::getWallCoordinates(int xPosition, int yPosition, bool isHorizontal)
{
	if (isHorizontal) {
		return glm::vec3(
			xPosition * 1.0f + 0.5f - 0.5f * xSize_, 
			yPosition * 1.0f - 0.5f * ySize_, 
			0.5f
		);
	}
	else {
		return glm::vec3(
			xPosition * 1.0f - 0.5f * xSize_, 
			yPosition * 1.0f + 0.5f - 0.5f * ySize_, 
			0.5f
		);
	}
}

}