#include "maze.hpp"
#include "maze_camera_mover.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>
#include <cmath>


namespace mazekit {

namespace {

static const float MARGIN_WIDTH = 0.4f;

}

MazeCameraMover::MazeCameraMover(mazekit::MazePtr maze) :
	CameraMover(),
	oldXPosition_(0),
	oldYPosition_(0),
	cameraPosition_(-1.5f, -5.0f, 0.5f)
{
	cameraRotation_ = glm::toQuat(
		glm::lookAt(
			cameraPosition_, 
			glm::vec3(0.0f, 0.0f, 0.5f), 
			glm::vec3(0.0f, 0.0f, 1.0f)
		)
	);
	maze_ = maze;
}

void MazeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void MazeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	const int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (state == GLFW_PRESS)
	{
		const double dx = xpos - oldXPosition_;
		const double dy = ypos - oldYPosition_;

		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * cameraRotation_;
		cameraRotation_ *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

		glm::vec3 upDir(0.0f, 0.0f, 1.0f);
		cameraRotation_ *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
	}

	oldXPosition_ = xpos;
	oldYPosition_ = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCameraMover::update(GLFWwindow* window, double dt)
{
	const float speed = 1.0f;

	const glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * cameraRotation_;

	const glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * cameraRotation_;

	const float _prevposZ = cameraPosition_.z;
	const float _prevPosX = cameraPosition_.x;
	const float _prevPosY = cameraPosition_.y;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		cameraPosition_ += forwDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		cameraPosition_ -= forwDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		cameraPosition_ -= rightDir * speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		cameraPosition_ += rightDir * speed * static_cast<float>(dt);
	}

	cameraPosition_.z = _prevposZ;

	if (checkCollision(glm::vec2(cameraPosition_.x, cameraPosition_.y))) {
		cameraPosition_.x = _prevPosX;
		cameraPosition_.y = _prevPosY;
	}

	_camera.viewMatrix = glm::toMat4(-cameraRotation_) * glm::translate(-cameraPosition_);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

bool MazeCameraMover::checkCollision(glm::vec2 position) {
	const float xShifted = position.x + 0.5f * maze_->width();
	const float yShifted = position.y + 0.5f * maze_->height();
	const float insideCellX = fmodf(xShifted, 1.0f);
	const float insideCellY = fmodf(yShifted, 1.0f);

	const int divX = static_cast<int>(floorf(xShifted));
	const int divY = static_cast<int>(floorf(yShifted));

	if (divX >= 0 && divX < maze_->width() && divY >= 0 && divY < maze_->height()) {
		if (maze_->mazeCells()[divY][divX]) {
			return true;
		}
		if (insideCellX < 0.5f * MARGIN_WIDTH) {
			if (insideCellY < 0.5f * MARGIN_WIDTH) {
				if (maze_->angleWalls()[divY][divX]) {
					return true;
				}
			}
			else if (insideCellY > 1.0f - 0.5f * MARGIN_WIDTH) {
				if (maze_->angleWalls()[divY][divX + 1]) {
					return true;
				}
			}
			else {
				if (maze_->verticalWalls()[divY][divX]) {
					return true;
				}
			}
		}
		else if (insideCellX > 1.0f - 0.5f * MARGIN_WIDTH) {
			if (insideCellY < 0.5f * MARGIN_WIDTH) {
				if (maze_->angleWalls()[divY + 1][divX]) {
					return true;
				}
			}
			else if (insideCellY > 1.0f - 0.5f * MARGIN_WIDTH) {
				if (maze_->angleWalls()[divY + 1][divX + 1]) {
					return true;
				}
			}
			else {
				if (maze_->verticalWalls()[divY][divX + 1]) {
					return true;
				}
			}
		}
		else {
			if (insideCellY < 0.5f * MARGIN_WIDTH) {
				if (maze_->horizontalWalls()[divY][divX]) {
					return true;
				}
			}
			else if (insideCellY > 1.0f - 0.5f * MARGIN_WIDTH) {
				if (maze_->horizontalWalls()[divY + 1][divX]) {
					return true;
				}
			}
		}

	}
	return false;
}

}